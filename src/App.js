
import './App.css';
import { Heading } from '@chakra-ui/react';
import Todolist from './components/TodoList';
import AddTodo from './components/AddTodo';
import {VStack, IconButton} from '@chakra-ui/react';
import {FaSun, FaMoon} from 'react-icons/fa'
import {useState, useEffect} from 'react';

function App() {
  const InitialTodos = [
    {
        id: 1,
        body: 'finish lunch'

    },
    {
        id: 1,
        body: 'finish react'

    }
];
const [todos, settodo] = useState(InitialTodos)
  function deleteTodo(id){
    const newTodos = todos.filter(todo => {
      return todo.id !== id;

    });
    settodo(newTodos)
  }
  return (
  <VStack p={4}>
    <IconButton icon={<FaSun />} isRound='true' size='lg' alignSelf="flex-end" />
    <Heading 
    mb="8" 
    fontWeight='extrabold'
    size='2xl'
    bgGradient='linear(to-r, red.500, pink.300, red.500)'
    bgClip="text"
    >To Do Application 
    </Heading>
    <Todolist todos={todos}/>
    <AddTodo/>

    </VStack>
    
  );
}

export default App;
