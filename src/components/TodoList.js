import React from 'react'
import {HStack, VStack, Text, IconButton, StackDivider, Spacer} from '@chakra-ui/react';
import {FaTrash} from 'react-icons/fa';
function TodoList({todos}) {
    return (

        <VStack 
        divider={<StackDivider/>}
        borderColor='gray.200'
        borderWidth='2px'
        p='4'
        borderRadius="lg"
        w="100%"
        maxW={{base: '90vm', sm:'80vm', lg:'50vw', xl:'40vm'}}
        alignItems="stretch"
        >
            {todos.map(todo =>(
                <HStack key={todo.id}>
                    <Text>{todo.body}</Text>
                    <Spacer />
                    <IconButton icon={<FaTrash />} isRound='true' />
                </HStack>
            ))}
        </VStack>
    );
}

export default TodoList
