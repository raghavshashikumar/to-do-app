import { Button } from '@chakra-ui/button'
import { Input } from '@chakra-ui/input'
import { HStack } from '@chakra-ui/layout'
import React from 'react'

function AddTodo() {
    function handleSubmit( e){

    }
    return (
        <form onSubmit={handleSubmit}>
            <HStack mt='8'>
                <Input variant="filled" placeholder="learn python"/>
                <Button colorScheme="red" px="8" type="submit" bgGradient="linear(to-r, red.500, pink.300, red.500 )">Add Todo</Button>
            </HStack>
            
        </form>
    )
}

export default AddTodo
